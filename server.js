'use strict';
const api = require('./api');
require('dotenv').config();

const express = require('express');
const path = require('path');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/report', async (req, res) => {
  var data = await api.main()
  res.send("This is the failed simulation count: " + JSON.stringify(data))
})


app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
